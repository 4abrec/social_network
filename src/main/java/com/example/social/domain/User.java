package com.example.social.domain;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.*;


@Entity
@Data
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;


    @Column(unique = true)
    private String login;
    private String password;


    @ManyToOne
    @JoinColumn(name = "role_id")
    private Role role;

}
