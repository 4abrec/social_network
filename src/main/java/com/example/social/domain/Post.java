package com.example.social.domain;


import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.*;
import java.sql.Date;
import java.util.Set;

@Entity
@Data
public class Post {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    private String heading;
    private String description;
    private String text;
    private String imageUrl;
    private String nameButton;
    private String urlButton;
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "archive",
    joinColumns = {@JoinColumn(name = "post_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "user_id", referencedColumnName = "id")})
    private Set<User> users;
    private Date actual;
}
