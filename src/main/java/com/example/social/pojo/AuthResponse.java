package com.example.social.pojo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.*;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "token"
})
@XmlRootElement(
        name = "AuthResponse",
        namespace = "http://com.example.social/pojo/dto/AuthUserDto")


@AllArgsConstructor
@NoArgsConstructor
public class AuthResponse {

    @XmlElement(
            name = "token",
            namespace = "http://com.example.social/pojo/AuthUserDto")
    private String token;
}
