package com.example.social.pojo.dto;

import lombok.Data;

import javax.xml.bind.annotation.*;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "name"
})
@XmlRootElement(
        name = "RoleDto",
        namespace = "http://com.example.social/pojo/dto/RoleDto")
@Data
public class RoleDto {

    @XmlElement(
            name = "name",
            namespace = "http://com.example.social/pojo/dto/RoleDto")
    private String name;
}
