package com.example.social.pojo.dto;

import lombok.Data;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "login",
        "password"
})
@XmlRootElement(
        name = "RegistrationUserDto",
        namespace = "http://com.example.social/pojo/dto/RegistrationUserDto")
@Data
public class RegistrationUserDto {

    @XmlElement(
            name = "login",
            namespace = "http://com.example.social/pojo/dto/RegistrationUserDto")
    private String login;

    @XmlElement(
            name = "password",
            namespace = "http://com.example.social/pojo/dto/RegistrationUserDto")
    private String password;


}