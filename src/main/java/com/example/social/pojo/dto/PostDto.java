package com.example.social.pojo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.*;
import java.sql.Date;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "heading",
        "description",
        "text",
        "actual"
})
@XmlRootElement(
        name = "PostDto",
        namespace = "http://com.example.social/pojo/dto/PostDto")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PostDto {

    @XmlElement(
            name = "heading",
            namespace = "http://com.example.social/pojo/dto/PostDto")
    private String heading;

    @XmlElement(
            name = "description",
            namespace = "http://com.example.social/pojo/dto/PostDto")
    private String description;

    @XmlElement(
            name = "text",
            namespace = "http://com.example.social/pojo/dto/PostDto")
    private String text;

    @XmlElement(
            name = "text",
            namespace = "http://com.example.social/pojo/dto/PostDto")
    private Date actual;

}
