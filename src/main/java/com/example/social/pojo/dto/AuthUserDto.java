package com.example.social.pojo.dto;

import lombok.Data;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "login",
        "password"
})
@XmlRootElement(
        name = "AuthUserDto",
        namespace = "http://com.example.social/pojo/dto/AuthUserDto")
@Data
public class AuthUserDto {

    @XmlElement(
            name = "login",
            namespace = "http://com.example.social/pojo/dto/AuthUserDto")
    private String login;

    @XmlElement(
            name = "password",
            namespace = "http://com.example.social/pojo/dto/AuthUserDto")
    private String password;


}