package com.example.social.config;

import com.example.social.domain.User;
import com.example.social.repos.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;


@Component
@RequiredArgsConstructor
public class CustomUserDetailService implements UserDetailsService {

    private final UserRepository userRepository;

    @Override
    public CustomUserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        User userEntity = userRepository.findByLogin(login);
        return CustomUserDetails.fromUserEntityToCustomUserDetails(userEntity);
    }
}
