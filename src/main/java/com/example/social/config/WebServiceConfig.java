package com.example.social.config;

import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.ws.config.annotation.EnableWs;
import org.springframework.ws.transport.http.MessageDispatcherServlet;
import org.springframework.ws.wsdl.wsdl11.DefaultWsdl11Definition;
import org.springframework.ws.wsdl.wsdl11.SimpleWsdl11Definition;
import org.springframework.xml.xsd.SimpleXsdSchema;
import org.springframework.xml.xsd.XsdSchema;


@EnableWs
@Configuration
public class WebServiceConfig {

    @Bean
    public ServletRegistrationBean<MessageDispatcherServlet> messageDispatcherServlet(ApplicationContext applicationContext){
        MessageDispatcherServlet messageDispatcherServlet = new MessageDispatcherServlet();
        messageDispatcherServlet.setApplicationContext(applicationContext);
        messageDispatcherServlet.setTransformWsdlLocations(true);
        return new ServletRegistrationBean<>(messageDispatcherServlet, "/ws/*");
    }

    @Bean(name = "registration")
    public SimpleWsdl11Definition registrationWsdl11Definition() {
        SimpleWsdl11Definition simpleWsdl11Definition = new SimpleWsdl11Definition ();
        simpleWsdl11Definition.setWsdl(new ClassPathResource("soap.wsdl/registration.wsdl"));
        return simpleWsdl11Definition;
    }

    @Bean(name = "auth")
    public SimpleWsdl11Definition authWsdl11Definition() {
        SimpleWsdl11Definition simpleWsdl11Definition = new SimpleWsdl11Definition ();
        simpleWsdl11Definition.setWsdl(new ClassPathResource("soap.wsdl/auth.wsdl"));
        return simpleWsdl11Definition;
    }

    @Bean(name = "post")
    public SimpleWsdl11Definition postWsdl11Definition() {
        SimpleWsdl11Definition simpleWsdl11Definition = new SimpleWsdl11Definition ();
        simpleWsdl11Definition.setWsdl(new ClassPathResource("soap.wsdl/post.wsdl"));
        return simpleWsdl11Definition;
    }

    @Bean(name = "role")
    public SimpleWsdl11Definition roleWsdl11Definition() {
        SimpleWsdl11Definition simpleWsdl11Definition = new SimpleWsdl11Definition ();
        simpleWsdl11Definition.setWsdl(new ClassPathResource("soap.wsdl/role.wsdl"));
        return simpleWsdl11Definition;
    }

}
