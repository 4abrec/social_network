package com.example.social.exception;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;

@Data
@AllArgsConstructor
public class ExceptionStructure {

    private int status;
    private String error;
    private String message;
    private Date date;
    private String URL;

    public static ExceptionStructure createException(RequestException requestException, String path) {
        return new ExceptionStructure(requestException.getHttpStatus().value(), requestException.getHttpStatus().name(),
                requestException.getMessage(), new Date(System.currentTimeMillis()), path);
    }
}
