package com.example.social.exception;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;

@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(RequestException.class)
    public ResponseEntity<ExceptionStructure> handleException(RequestException requestException, HttpServletRequest webRequest) {
        return ResponseEntity.status(requestException.getHttpStatus())
                .body(ExceptionStructure.createException(requestException, webRequest.getRequestURI()));
    }
}
