package com.example.social.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public class RequestException extends RuntimeException{

    private HttpStatus httpStatus;

    public RequestException(){
    }

    public RequestException(String msg, HttpStatus httpStatus){
        super(msg);
        this.httpStatus = httpStatus;
    }

    public RequestException(String msg, Throwable cause){
        super(msg, cause);
    }

    public RequestException(Throwable cause){
        super(cause);
    }
}
