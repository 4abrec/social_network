package com.example.social.client.config;

import com.example.social.client.exception.RestTemplateErrorHandler;
import com.example.social.client.log.RequestResponseLogInterceptor;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.tools.javac.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.cloud.openfeign.support.PageJacksonModule;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.time.Duration;
import java.util.Collections;

@Configuration
@RequiredArgsConstructor
public class CustomRestTemplate {

    private final RequestResponseLogInterceptor requestResponseLoggingInterceptor;
    private final RestTemplateErrorHandler restTemplateErrorHandler;
    private final Duration fooConnectTimeout = Duration.ofMillis(3000);
    private final Duration fooReadTimeout= Duration.ofMillis(3000);

    private ObjectMapper createObjectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
        objectMapper.registerModule(new PageJacksonModule());
        return objectMapper;
    }

    private MappingJackson2HttpMessageConverter createMappingJacksonHttpMessageConverter() {
        MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
        converter.setObjectMapper(createObjectMapper());
        return converter;
    }

    @Bean
    public RestTemplate create(RestTemplateBuilder builder){
        return builder
                .errorHandler(restTemplateErrorHandler)
                .interceptors(Collections.singleton(requestResponseLoggingInterceptor))
                .messageConverters(List.of(new FormHttpMessageConverter(), createMappingJacksonHttpMessageConverter()))
                .setReadTimeout(fooReadTimeout)
                .setConnectTimeout(fooConnectTimeout)
                .build();
    }
}
