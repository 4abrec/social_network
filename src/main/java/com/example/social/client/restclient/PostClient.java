package com.example.social.client.restclient;

import com.example.social.domain.Post;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import java.util.Collections;
import java.util.List;

@RequiredArgsConstructor
@Component
public class PostClient {

    private static final String GET_ALL_POST_API = "http://localhost:8080/api/posts/get_all";
    private static final String ADD_POST = "http://localhost:8080/api/posts/add";
    private static final String UPDATE_POST = "http://localhost:8080/api/posts/update";
    private static final String DELETE_BY_ID = "http://localhost:8080/api/posts/";
    private static final String FIND_BY_ID = "http://localhost:8080/api/posts/find/";

    private final RestTemplate restTemplate;

    public List<Post> getAllPosts(){
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        HttpEntity<String> entity = new HttpEntity<>("parameters", httpHeaders);
        ResponseEntity<List<Post>> storyResponse = restTemplate.exchange(
                GET_ALL_POST_API,
                HttpMethod.GET,
                entity,
                new ParameterizedTypeReference<List<Post>>() {});
        return storyResponse.getBody();
    }

    public Post addPost(Post post){
        ResponseEntity<Post> postResponseEntity = restTemplate.postForEntity(ADD_POST, post, Post.class);
        return  postResponseEntity.getBody();
    }

    public Post updatePost(Post post){
        ResponseEntity<Post> postResponseEntity = restTemplate.postForEntity(UPDATE_POST, post, Post.class);
        return  postResponseEntity.getBody();
    }

    public void deleteById(String id){
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(DELETE_BY_ID).queryParam("id", id);
        HttpEntity<?> entity = new HttpEntity<>(headers);
        restTemplate.exchange(builder.toUriString(),
                HttpMethod.DELETE,
                entity,
                Object.class);
    }

    public Post findById(String id){
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(FIND_BY_ID)
                .queryParam("id", id);

        ResponseEntity<Post> postResponseEntity = restTemplate.getForEntity(
                builder.toUriString(),
                Post.class);
        return postResponseEntity.getBody();
    }
}
