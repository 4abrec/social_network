package com.example.social.service;
import com.example.social.domain.Post;
import com.example.social.pojo.dto.PostDto;

import java.util.List;

public interface PostService {

    Post getById(String id);
    void deleteById(String id);
    void savePost(Post post);
    void addToArchive(String post_id, String user_id);
    void removeFromArchive(String post_id, String user_id);
    void view(String id_post);
    Post toPost(PostDto postDto);
    PostDto update(Post oldPost, PostDto newPostDto);
    List<Post> getPosts(Integer limit, String category);
    List<Post> getAll();
}
