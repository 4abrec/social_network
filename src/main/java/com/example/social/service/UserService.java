package com.example.social.service;
import com.example.social.domain.User;
import com.example.social.pojo.dto.RegistrationUserDto;

import java.util.List;

public interface UserService {

    User getById(String id);
    List<User> getAll();
    void deleteById(String id);
    void saveUser(User user);
    void registerUser(RegistrationUserDto registrationUserDto, User user);
    User getByLogin(String login);

    User getByLoginAndPassword(String login, String password);

}
