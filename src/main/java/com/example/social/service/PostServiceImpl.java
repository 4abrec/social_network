package com.example.social.service;


import com.example.social.config.jwt.JwtProvider;
import com.example.social.domain.Post;
import com.example.social.domain.User;
import com.example.social.pojo.dto.PostDto;
import com.example.social.repos.PostRepository;
import com.example.social.repos.UserRepository;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class PostServiceImpl implements PostService{

    private final PostRepository postRepository;
    private final UserService userService;
    private final JwtProvider jwtProvider;

    @Override
    public Post getById(String id) {
        return postRepository.findById(id).orElse(null);
    }

    @Override
    public void deleteById(String id) {
        postRepository.deleteById(id);
    }

    @Override
    public void savePost(Post post) {
        postRepository.save(post);
    }

    @Override
    public void addToArchive(String post_id, String user_id){
        Post post = postRepository.findById(post_id).orElse(null);
        if(post != null){
            Set<User> users = post.getUsers();
            users.add(userService.getById(user_id));
            post.setUsers(users);
            postRepository.save(post);
        }

    }

    @Override
    public void removeFromArchive(String post_id, String user_id) {
        Post post = postRepository.findById(post_id).orElse(null);
        if(post != null){
            Set<User> users = post.getUsers();
            users.remove(userService.getById(user_id));
            post.setUsers(users);
            postRepository.save(post);
        }
    }

    @Override
    public void view(String id_post) {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
        String token = request.getHeader("Authorization").split(" ")[1];
        String login = jwtProvider.getLoginFromToken(token);
        User user = userService.getByLogin(login);
        Post post = postRepository.findById(id_post).orElse(null);
        assert post != null;
        Set<User> userList = post.getUsers();
        userList.add(user);
        post.setUsers(userList);
        postRepository.save(post);
    }

    @Override
    public List<Post> getPosts(Integer limit, String category) {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
        String token = request.getHeader("Authorization").split(" ")[1];
        String login = jwtProvider.getLoginFromToken(token);
        User user = userService.getByLogin(login);
        List<Post> posts = new ArrayList<>();
        List<Post> allPosts = postRepository.findAll();
        if(category.equals("new")){
            for(Post post: allPosts){
                if (!post.getUsers().contains(user))
                    posts.add(post);
            }
        }
        else if(category.equals("archive")){
            for(Post post: allPosts){
                if (post.getUsers().contains(user))
                    posts.add(post);
            }
        }
        else{
            posts = allPosts;
        }
        if (limit == -1){
            return posts;
        }
        else if(limit > posts.size()){
            limit = posts.size();
        }
        return posts.subList(0, limit);
    }

    @Override
    public Post toPost(PostDto postDto) {
        Post post = new Post();
        post.setHeading(postDto.getHeading());
        post.setDescription(postDto.getDescription());
        post.setText(postDto.getText());
        post.setActual(postDto.getActual());
        return post;
    }

    @Override
    public PostDto update(Post oldPost, PostDto newPostDto) {
        oldPost.setHeading(newPostDto.getHeading());
        oldPost.setDescription(newPostDto.getDescription());
        oldPost.setText(newPostDto.getText());;
        oldPost.setActual(newPostDto.getActual());
        postRepository.save(oldPost);
        return new PostDto(oldPost.getHeading(), oldPost.getDescription(), oldPost.getText(), oldPost.getActual());
    }

    @Override
    public List<Post> getAll() {
        return postRepository.findAll();
    }
}
