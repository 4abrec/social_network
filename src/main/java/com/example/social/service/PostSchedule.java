package com.example.social.service;
import com.example.social.domain.Post;
import com.example.social.domain.User;
import com.example.social.repos.PostRepository;
import com.example.social.repos.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import java.sql.Date;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.Set;

@Component
@RequiredArgsConstructor
public class PostSchedule {

    private final PostRepository postRepository;
    private final UserRepository userRepository;

    @Scheduled(fixedRate = 300000)
    public void checkAndAddToArchive(){
        List<Post> posts = postRepository.findAll();
        for(Post post: posts){
            Date someSqlDate = post.getActual();
            LocalDate localDate = someSqlDate.toLocalDate();
            LocalDate today = LocalDate.now(ZoneId.of("Africa/Addis_Ababa")); //Московский часовой пояс
            if(localDate.isBefore(today)){
                Set<User> users = post.getUsers();
                users.addAll(userRepository.findAll());
                postRepository.save(post);
            }
        }
    }
}
