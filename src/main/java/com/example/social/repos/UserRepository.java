package com.example.social.repos;

import com.example.social.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, String> {

    User findByLogin(String login);
    User findByLoginAndPassword(String login, String password);
}
