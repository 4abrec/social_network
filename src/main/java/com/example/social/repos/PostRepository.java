package com.example.social.repos;

import com.example.social.domain.Post;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PostRepository extends JpaRepository <Post, String> {
//    List<Post> findBySeen(Boolean seen);
}
