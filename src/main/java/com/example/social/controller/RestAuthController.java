package com.example.social.controller;


import com.example.social.config.jwt.JwtProvider;
import com.example.social.domain.User;
import com.example.social.pojo.AuthResponse;
import com.example.social.pojo.dto.AuthUserDto;
import com.example.social.pojo.dto.RegistrationUserDto;
import com.example.social.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
public class RestAuthController {

    private final JwtProvider jwtProvider;
    private final UserService userService;


    @PostMapping("/signup")
    public String registerUser(@RequestBody @Valid RegistrationUserDto registrationUserDto) {
        User user = new User();
        userService.registerUser(registrationUserDto, user);
        userService.saveUser(user);
        return "OK";
    }

    @PostMapping("/auth")
    public AuthResponse auth(@RequestBody AuthUserDto authUserDto) {
        User user = userService.getByLoginAndPassword(authUserDto.getLogin(), authUserDto.getPassword());
        String token = jwtProvider.generateToken(user.getLogin());
        return new AuthResponse(token);
    }
}
