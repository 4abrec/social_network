package com.example.social.controller;

import com.example.social.domain.Post;
import com.example.social.domain.User;
import com.example.social.service.PostService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping("/api/posts")
@RequiredArgsConstructor

public class RestPostController {

    private final PostService postService;

    //создание новости
    @RequestMapping(value = "/add", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Post> addPost(@RequestBody @Validated Post post){
        postService.savePost(post);
        return new ResponseEntity<>(post, HttpStatus.CREATED);
    }



    //добавление в архив
    @RequestMapping(value = "/add_to_archive", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public void addPostToArchive(@RequestParam(name = "post_id") String post_id,
                                 @RequestParam(name = "user_id") String user_id){
        postService.addToArchive(post_id, user_id);
    }

    //удаление из архива
    @RequestMapping(value = "/remove_from_archive", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public void removeFromArchive(@RequestParam(name = "post_id") String post_id,
                                 @RequestParam(name = "user_id") String user_id){
        postService.removeFromArchive(post_id, user_id);
    }

    //просмотр новости
    @RequestMapping(value = "/view", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Post> viewPost(@RequestParam(name = "id_post") String id_post){
        postService.view(id_post);
        return new ResponseEntity<>(postService.getById(id_post), HttpStatus.OK);
    }

    //получение списка новостей, в зависимости от параметров
    @RequestMapping(value = "", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Post>> getLimitFilterPosts(@RequestParam(name = "limit", defaultValue = "-1")Integer limit,
                                                          @RequestParam(name = "category", defaultValue = "all")String category){
        List<Post> posts = postService.getPosts(limit, category);
        return new ResponseEntity<>(posts, HttpStatus.OK);
    }
    @RequestMapping(value = "/get_all", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Post>> getAllPost(){
        return new ResponseEntity<>(postService.getAll(), HttpStatus.OK);
    }

    @DeleteMapping("")
    public void deletePost(@RequestParam(name = "id") String id){
        postService.deleteById(id);
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Post> updatePost(@RequestBody @Validated Post post){
        postService.savePost(post);
        return new ResponseEntity<>(post, HttpStatus.OK);
    }

    @GetMapping("/find")
    public ResponseEntity<Post> findById (@RequestParam(name = "id") String id)
    {
        Post post = postService.getById(id);
        return new ResponseEntity<>(post, HttpStatus.OK);
    }


}
