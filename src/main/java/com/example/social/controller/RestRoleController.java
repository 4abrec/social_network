package com.example.social.controller;

import com.example.social.config.jwt.JwtProvider;
import com.example.social.domain.Role;
import com.example.social.repos.RoleRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("api/role")
@RequiredArgsConstructor
public class RestRoleController {

    private final JwtProvider jwtProvider;

    private final RoleRepository roleRepository;

    @RequestMapping(value = "/add", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public void addPost(@RequestBody @Validated Role role){
        roleRepository.save(role);
    }
}
