package com.example.social.edpoints.post;

import com.example.social.pojo.dto.PostDto;
import lombok.Data;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "postDto",
        "id"
})
@XmlRootElement(name = "UpdatePost", namespace = "http://com.example.social/pojo/dto/PostDto")
@Data
public class UpdatePost {
    @XmlElement(name = "PostDto", namespace = "http://com.example.social/pojo/dto/PostDto")
    private PostDto postDto;

    @XmlElement(name = "id", namespace = "http://com.example.social/pojo/dto/PostDto")
    private String id;
}