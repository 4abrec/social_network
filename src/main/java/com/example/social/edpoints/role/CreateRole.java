package com.example.social.edpoints.role;


import com.example.social.pojo.dto.RoleDto;
import lombok.Data;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "roleDto"
})

@XmlRootElement(name = "CreateRole", namespace = "http://com.example.social/pojo/dto/RoleDto")
@Data
public class CreateRole {

    @XmlElement(name = "RoleDto", namespace = "http://com.example.social/pojo/dto/RoleDto")
    private RoleDto roleDto;
}