package com.example.social.edpoints;

import com.example.social.domain.Post;
import com.example.social.edpoints.post.CreatePost;
import com.example.social.edpoints.post.UpdatePost;
import com.example.social.pojo.dto.PostDto;
import com.example.social.service.PostService;
import lombok.RequiredArgsConstructor;
import org.springframework.ws.server.endpoint.annotation.*;

@RequiredArgsConstructor
@Endpoint
public class PostEndpoint {

    private static final String NAMESPACE_URI = "http://com.example.social/pojo/dto/PostDto";

    private final PostService postService;

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "FindByIdRequest")
    @Namespace(uri = NAMESPACE_URI, prefix = "tns")
    @ResponsePayload
    public PostDto findById(@XPathParam(value = "tns:id") String id){
        Post post = postService.getById(id);
        return new PostDto(post.getHeading(),post.getDescription(),post.getText(), post.getActual());
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "ViewRequest")
    @Namespace(uri = NAMESPACE_URI, prefix = "tns")
    @ResponsePayload
    public void view(@XPathParam(value = "tns:id") String id){
        postService.view(id);;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "CreatePost")
    @ResponsePayload
    public void create(@RequestPayload CreatePost createPost){
        Post post = postService.toPost(createPost.getPostDto());
        postService.savePost(post);
    }


    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "UpdatePost")
    @ResponsePayload
    public PostDto update(@RequestPayload UpdatePost updatePost){
        Post post = postService.getById(updatePost.getId());
        return postService.update(post, updatePost.getPostDto());
    }



}
