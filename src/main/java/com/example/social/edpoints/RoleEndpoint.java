package com.example.social.edpoints;

import com.example.social.domain.Role;
import com.example.social.edpoints.role.CreateRole;
import com.example.social.pojo.dto.RoleDto;
import com.example.social.repos.RoleRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

@Endpoint
@RequiredArgsConstructor
public class RoleEndpoint {

    private static final String NAMESPACE_URI = "http://com.example.social/pojo/dto/RoleDto";

    private final RoleRepository roleRepository;

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "CreateRole")
    @ResponsePayload
    public void CreateRole(@RequestPayload CreateRole createRole){
        RoleDto roleDto = createRole.getRoleDto();
        Role role = new Role();
        role.setName(roleDto.getName());
        roleRepository.save(role);
    }

}
