package com.example.social.edpoints;

import com.example.social.config.jwt.JwtProvider;
import com.example.social.domain.User;
import com.example.social.pojo.AuthResponse;
import com.example.social.pojo.dto.AuthUserDto;
import com.example.social.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;


@Endpoint
@RequiredArgsConstructor
public class AuthEndpoint {

    private static final String NAMESPACE_URI = "http://com.example.social/pojo/dto/AuthUserDto";

    private final UserService userService;
    private final JwtProvider jwtProvider;


    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "AuthUserDto")
    @ResponsePayload
    public AuthResponse auth(@RequestPayload AuthUserDto authUserDto){
        User user = userService.getByLoginAndPassword(authUserDto.getLogin(), authUserDto.getPassword());
        String token = jwtProvider.generateToken(user.getLogin());
        return new AuthResponse(token);
    }
}
