package com.example.social.edpoints;

import com.example.social.domain.User;
import com.example.social.pojo.dto.RegistrationUserDto;
import com.example.social.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;

@Endpoint
@RequiredArgsConstructor
public class RegistrationEndpoint {

    private static final String NAMESPACE_URI = "http://com.example.social/pojo/dto/RegistrationUserDto";

    private final UserService userService;

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "RegistrationUserDto")
    private void registration(@RequestPayload RegistrationUserDto registrationUserDto){
        User user = new User();
        userService.registerUser(registrationUserDto, user);
        userService.saveUser(user);
    }
}
